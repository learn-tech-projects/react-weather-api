import React, { Component } from 'react';

import './App.css';

import {WeatherMain} from './components/WeatherMain.js'
import {WeatherCard} from './components/WeatherCard.js'

export default class App extends Component {
	
	constructor(props){
			super(props)
	
			this.state = {
				api_key:"10d42c84a77154b5235c3a550b1981b8",
				city_id:'1538317',
				city:"",
				city_country:"",
				data:[],
			}

			this.changeCity = this.changeCity.bind(this)
	}

	UNSAFE_componentWillMount () {
		fetch('http://api.openweathermap.org/data/2.5/forecast?id='+this.state.city_id+"&appid="+this.state.api_key+"&units=metric",{
		  method: "GET"
		}).then((response)=>response.json()
		, (error)=>{console.log(error)
		}).then((data)=>{
			this.setState({
				city:data.city.name,
				city_country:data.city.country,
				data:data.list
			})
		})
	}	

	changeCity(id){
		this.setState({
			city_id:id
		},function(){
			fetch('http://api.openweathermap.org/data/2.5/forecast?id='+this.state.city_id+"&appid="+this.state.api_key+"&units=metric",{
			  method: "GET"
			}).then((response)=>response.json()
			, (error)=>{console.log(error)
			}).then((data)=>{

				this.setState({
					city:data.city.name,
					city_country:data.city.country,
					data:data.list
				})
			})			
		})

				
	}

	render() {

		const cards = this.state.data.filter((e,index)=>index!==0&&index<7?true:false).map((item,index)=>{
			return <WeatherCard key={index} data={item}></WeatherCard>
		})

		return (
			<div className="App">
			

				<div className="App__cities">
					<p className="App__cities-item" onClick={(e)=>this.changeCity('1526273',e) }>Astana</p>
					<p className="App__cities-item" onClick={(e)=>this.changeCity('1526395',e) }>Almaty Qalasy</p>
					<p className="App__cities-item" onClick={(e)=>this.changeCity('1518980',e) }>Shymkent</p>
					<p className="App__cities-item" onClick={(e)=>this.changeCity('610613',e) }>Aqsay</p>
					<p className="App__cities-item" onClick={(e)=>this.changeCity('1526269',e) }>Aqtau</p>
					<p className="App__cities-item" onClick={(e)=>this.changeCity('610530',e) }>Atrau</p>
					<p className="App__cities-item" onClick={(e)=>this.changeCity('1519422',e) }>Semey</p>
					<p className="App__cities-item" onClick={(e)=>this.changeCity('610611',e) }>Aqtobe</p>
					<p className="App__cities-item" onClick={(e)=>this.changeCity('1519928',e) }>Qostanay</p>
					<p className="App__cities-item" onClick={(e)=>this.changeCity('1519922',e) }>Qyzylorda</p>
					<p className="App__cities-item" onClick={(e)=>this.changeCity('1520316',e) }>Ust-Kamenogorsk</p>
					<p className="App__cities-item" onClick={(e)=>this.changeCity('1516589',e) }>Zhezqazghan</p>
					<p className="App__cities-item" onClick={(e)=>this.changeCity('1524325',e) }>Ekibastuz</p>
					<p className="App__cities-item" onClick={(e)=>this.changeCity('1518262',e) }>Temirtau</p>
					<p className="App__cities-item" onClick={(e)=>this.changeCity('1521315',e) }>Lisakovsk</p>
					<p className="App__cities-item" onClick={(e)=>this.changeCity('1537939',e) }>Stepnogorsk</p>
					<p className="App__cities-item" onClick={(e)=>this.changeCity('1518543',e) }>Taldyqorghan</p>
					<p className="App__cities-item" onClick={(e)=>this.changeCity('1523400',e) }>Qaraghandy</p>
					<p className="App__cities-item" onClick={(e)=>this.changeCity('1518542',e) }>Taldyqorghan</p>
				</div>
				
				<h1 className="App__title">Weather <span>in</span> {this.state.city} ( {this.state.city_country} )</h1>
				
				<div className="App__wrap">

					<div className="App__wrap-left">
						{this.state.data.length>1?<WeatherMain data={this.state.data[0]}></WeatherMain>:''}
					</div>

					<div className="App__wrap-right">
						{this.state.data.length>1?cards:''}
					</div>


				</div>
				
				<h5 className="App__footer-text">created by <span>s1n1gam1</span></h5>
			</div>
		);
	}
}

