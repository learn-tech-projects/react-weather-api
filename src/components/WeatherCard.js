import "./WeatherCard.css";

import React, { Component } from 'react';

export class WeatherCard extends Component {

	render() {

		let days = ["Mon","Tue","Wedn","Thu","Fri","Sat","Sun"]
		let date = new Date(this.props.data.dt*10000)

		console.log(date)

		return (
			<div className="WeatherCard">	
				<div className="WeatherCard__header">
					<img src={'/img/weather-icons/'+this.props.data.weather[0].icon+".svg"} alt="" className="WeatherCard__icon"/>
				</div>

				<div className="WeatherCard__content">
					<p className="WeatherCard__day">{ days[date.getDay()] }</p>
				</div>

				<div className="WeatherCard__footer">
					<p className="WeatherCard__temp">{this.props.data.main.temp}&deg;C</p>
					<p className="WeatherCard__temp">{this.props.data.weather[0].description}</p>

				</div>

			</div>
		);
	}
}
