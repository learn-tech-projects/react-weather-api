import './WeatherMain.css';

import React, { Component } from 'react';

export class WeatherMain extends Component {

	render() {
		
		let days = ["Mon","Tue","Wedn","Thu","Fri","Sat","Sun"]
		let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		let date = new Date(this.props.data.dt_txt)

		let month = months[ date.getMonth() ]
		let dayInMonth = date.getDate()

		return (



			<div className="WeatherMain">

					<div className="WeatherMain__header">
						<img src={'/img/weather-icons/'+this.props.data.weather[0].icon+".svg"} alt="" className="WeatherMain__icon"/>
					</div>

					<div className="WeatherMain__content">
						<p className="WeatherMain__day">{days[date.getDay()]}</p>
						<p className="WeatherMain__date">{month+' '+dayInMonth}</p>
						<p className="WeatherMain__values"> {this.props.data.main.temp}&deg;C</p>
						
					</div>

					<div className="WeatherMain__footer">
						<p className="WeatherMain__text">Atmospheric pressure {this.props.data.main.pressure } hPa</p>
						<p className="WeatherMain__text">{this.props.data.weather[0].description}</p>
						<p className="WeatherMain__text">{this.props.data.main.temp_min}&deg;C / {this.props.data.main.temp_max}&deg;C</p>
						<p className="WeatherMain__text">Humidity {this.props.data.main.humidity }%</p>
					</div>


			</div>
		);
	}
}


